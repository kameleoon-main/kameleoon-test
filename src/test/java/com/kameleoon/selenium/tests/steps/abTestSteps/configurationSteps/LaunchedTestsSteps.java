package com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps;

import com.kameleoon.selenium.tests.pages.abTestPage.configurationPage.LaunchedTestsPage;

/**
 * Created by roman on 14.03.16.
 */
public class LaunchedTestsSteps {
    private LaunchedTestsPage launchedTestsPage;
    public LaunchedTestsSteps() {
        launchedTestsPage = new LaunchedTestsPage();
    }

    public String getTitleActionText(){
        return launchedTestsPage.getTitleActionText().getText();
    }
}
