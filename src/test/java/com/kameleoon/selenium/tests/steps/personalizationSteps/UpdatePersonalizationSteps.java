package com.kameleoon.selenium.tests.steps.personalizationSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.UpdatePersonalizationPage;
import com.kameleoon.selenium.tests.steps.personalizationSteps.popups.PopupActivateConfirmSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.popups.PopupSaveConfirmSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.segmentBuilderSteps.CreateNewSegmentSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.segmentUpdatePersonalizationSteps.InsideContentSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 14.03.16.
 *
 */
public class UpdatePersonalizationSteps {
    private UpdatePersonalizationPage updatePersonalizationPage;
    public UpdatePersonalizationSteps() {
        updatePersonalizationPage = new UpdatePersonalizationPage();
    }

    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public void createNewPers(String name, String description){
        updatePersonalizationPage.getPersoNameInputText().sendKeys(name);
        updatePersonalizationPage.getPersoDescriptionInputText().sendKeys(description);
        TimeUtils.waitForSeconds(1);
    }

    public PopupSaveConfirmSteps clickSavePers(){
        updatePersonalizationPage.getPersoSaveButton().click();
        return page(PopupSaveConfirmSteps.class);
    }

    public PopupActivateConfirmSteps clickActivatePers(){
        updatePersonalizationPage.getPersoActivateButton().click();
        TimeUtils.waitForSeconds(1);
        return page(PopupActivateConfirmSteps.class);
    }

    public CreateNewSegmentSteps clickAddNewSegment(){
        updatePersonalizationPage.getAddNewSegmentButton().click();
        return page(CreateNewSegmentSteps.class);
    }

    public ReportingPersonalizationSteps clickAddNewGoal(){
        updatePersonalizationPage.getAddNewGoalButton().click();
        return page(ReportingPersonalizationSteps.class);
    }

    public InsideContentSteps clickInsideContentPages(){
        updatePersonalizationPage.getInsideContentPages().click();
        TimeUtils.waitForSeconds(1);
        return page(InsideContentSteps.class);
    }

    public void clickAlertClosePers(){
        updatePersonalizationPage.getAlertCloseButton().click();
    }

    public void chooseEngagementGoalItem(){
        updatePersonalizationPage.getSelectDropDownGoalList().click();
        TimeUtils.waitForSeconds(1);
        moveMouseToElement(updatePersonalizationPage.getEngagementGoalItem());
        updatePersonalizationPage.getEngagementGoalItem().click();
        TimeUtils.waitForSeconds(3);
    }


}
