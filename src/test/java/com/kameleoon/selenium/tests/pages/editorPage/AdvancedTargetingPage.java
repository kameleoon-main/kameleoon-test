package com.kameleoon.selenium.tests.pages.editorPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 28.03.16.
 *
 */
public class AdvancedTargetingPage extends BasePage {
    public AdvancedTargetingPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return validateTargetingButton.isDisplayed();
    }

    @FindBy(xpath = "((.//*[@id='kameleoonQxPopIn']//*[@class='qx-UDPopInFrame'])[2]" +
            "/*[contains(@qxclass,'qx.ui.container.Composite')]/*[contains(@qxclass,'qx.ui.form.Button')])[3]" +
            "/*[contains(@qxclass,'qx.ui.basic.Label')]")
    private WebElement validateTargetingButton;


    public WebElement getValidateTargetingButton() {
        return validateTargetingButton;
    }
}
