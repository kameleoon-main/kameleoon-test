package com.kameleoon.selenium.tests.configuration.properties;

import com.kameleoon.selenium.tests.configuration.TestsConfig;
import com.kameleoon.selenium.tests.webtestsbase.Browser;

/**
 * Created by roman on 11.03.16.
 *
 */
public class Properties {

    public static String getUrl(){
        String property = "serverUrl";
        String url = System.getProperty(property);
        if (url == null || url.isEmpty()) {
            url = "https://back-office.kameleoon-dev.net";
            System.out.println(property + "=" + url);
            return url;
        }
        else {
            System.out.println(property + "=" + url);
            return url;
        }
    }

    public static String getDbConnection(){
        String property = "dbUrl";
        String url = System.getProperty(property);
        if (url == null || url.isEmpty()) {
            url = "jdbc:mysql://localhost:3306/net_kameleoon-dev_kameleoon?user=kameleoon-dev&password=kameleoon_dev";
            System.out.println(property + "=" + url);
            return url;
        }
        else {
            System.out.println(property + "=" + url);
            return url;
        }
    }

    public static Browser getBrowserName(){
        String property = "browserName";
        String browserName = System.getProperty(property);
        Browser browser = null;
        if (browserName == null || browserName.isEmpty()) {
            TestsConfig testsConfig = new TestsConfig();
            browser = testsConfig.getBrowser();
            System.out.println(property + "=" + testsConfig.getBrowser().toString());
            return browser;
        }
        else {
            System.out.println(property + "=" + browserName);
            browser = Browser.getByName(browserName);
            return browser;
        }
    }


}
