package com.kameleoon.selenium.tests.pages.abTestPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 09.03.16.
 *
 */
public class PopupNewExperimentPage extends BasePage {

    public PopupNewExperimentPage(){
        super();
    }

    @Override
    public boolean isPageOpened() {
        try {
            return okButton.isDisplayed();
        }
        catch (NoSuchElementException e) {System.out.println("Element okButton is not found"); return false;}
    }

    @FindBy(xpath = ".//*[@id='popup-new-experiment']//button[contains(@class, 'green')]")
    private WebElement okButton;

    @FindBy(xpath = ".//*[@id='popup-new-experiment']//button[contains(@class, 'cancel')]")
    private WebElement cancelButton;

    @FindBy(xpath = ".//*[@id='popup-new-experiment']//*[contains(@class, 'header')]")
    private WebElement headerText;

    @FindBy(xpath = ".//*[@id='popup-new-experiment']//*[contains(@name, 'name')]")
    private WebElement nameTestInputText;

    @FindBy(xpath = ".//*[@id='newExperimentBaseURL']")
    private WebElement urlTestInputText;

    @FindBy(xpath = ".//*[@id='popup-new-experiment']//*[contains(@class, 'test_sites')]")
    private WebElement associatedSiteMenu;


    public WebElement getOkButton(){
        return okButton;
    }

    public WebElement getCancelButton(){
        return cancelButton;
    }

    public WebElement getHeaderText(){
        return headerText;
    }

    public WebElement getNameTestInputText(){
        return nameTestInputText;
    }

    public WebElement getUrlTestInputText(){
        return urlTestInputText;
    }

    public WebElement getAssociatedSiteMenu(){
        return associatedSiteMenu;
    }


}
