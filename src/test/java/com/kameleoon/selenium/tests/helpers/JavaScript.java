package com.kameleoon.selenium.tests.helpers;

import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 17.03.16.
 *
 */
public class JavaScript {

    private static String code = null;

    public static void setCode(String code){
        JavaScript.code = code;
    }

    public static String getCode(){
        return code;
    }

    private static void javascriptExecutor(){
        JavascriptExecutor js = (JavascriptExecutor) WebDriverFactory.getDriver();
        js.executeScript("if (! window.kameleoonInjector)\n" +
                "{\n" +
                "   window.kameleoonInjector = true;\n" +
                "   var headNode = document.getElementsByTagName(\"head\")[0];\n" +
                "   var node = document.createElement(\"script\");\n" +
                "   node.setAttribute(\"type\", \"text/javascript\");\n" +
                "   node.setAttribute(\"charset\", \"utf-8\");\n" +
                "   node.innerHTML = \"window.Kameleoon = undefined;\";\n" +
                "   headNode.appendChild(node);\n" +
                "   node = document.createElement(\"script\");\n" +
                "   node.setAttribute(\"type\", \"text/javascript\");\n" +
                "   node.setAttribute(\"charset\", \"utf-8\");\n" +
                "   node.setAttribute(\"src\", \"http://static.kameleoon-dev.net/css/customers/"+ getCode() +"/0/kameleoon.js\");\n" +
                "   headNode.appendChild(node);\n" +
                "}");
    }

    /** Run javaScript */
    public static  <PageObjectClass> PageObjectClass launchEditor(Class<PageObjectClass> pageObjectClass) {
        javascriptExecutor();
        TimeUtils.waitForSeconds(10);
        try {
            return page(pageObjectClass.getConstructor().newInstance());
        }
        catch (Exception e) {
            throw new RuntimeException("Failed to create new instance of " + pageObjectClass, e);
        }
    }


}
