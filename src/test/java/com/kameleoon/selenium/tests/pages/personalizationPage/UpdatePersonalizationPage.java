package com.kameleoon.selenium.tests.pages.personalizationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 14.03.16.
 *
 */
public class UpdatePersonalizationPage extends BasePage {
    public UpdatePersonalizationPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getPersoNameInputText().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'personalization-name') and @type='text']")
    private WebElement persoNameInputText;

    @FindBy(xpath = ".//*[contains(@class, 'personalization-description') and @type='text']")
    private WebElement persoDescriptionInputText;

    @FindBy(xpath = ".//footer//*[contains(@class, 'bleu')]")
    private WebElement persoSaveButton;

    @FindBy(xpath = ".//footer//*[contains(@class, 'green')]")
    private WebElement persoActivateButton;

    @FindBy(xpath = ".//*[contains(@class, 'create-segment')]")
    private WebElement addNewSegmentButton;

    @FindBy(xpath = ".//*[contains(@class, 'create-goal')]")
    private WebElement addNewGoalButton;

    @FindBy(xpath = ".//*[contains(@class, 'perso-alert-close')]")
    private WebElement alertCloseButton;

    //-- Segments --//
    @FindBy(xpath = ".//*[contains(@class,'inpage')]//*[contains(@class,'inpage-container-header')]")
    private WebElement insideContentPages;
    //-------------//

    //-- Selection of main goal --//
    @FindBy(xpath = ".//*[contains(@class, 'personalization-goal')]//*[contains(@class, 'selectize-input')]")
    private WebElement selectDropDownGoalList;

    @FindBy(xpath = ".//*[contains(@class, 'personalization-goal')]//*[@class='selectize-dropdown-content']/*[1]")
    private WebElement engagementGoalItem;
    //---------------------------//



    public WebElement getPersoNameInputText(){
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,persoNameInputText);
        return persoNameInputText;
    }

    public WebElement getPersoDescriptionInputText() {
        return persoDescriptionInputText;
    }

    public WebElement getPersoSaveButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,persoSaveButton);
        return persoSaveButton;
    }

    public WebElement getAddNewSegmentButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,addNewSegmentButton);
        return addNewSegmentButton;
    }

    public WebElement getInsideContentPages() {
        return insideContentPages;
    }

    public WebElement getPersoActivateButton() {
        return persoActivateButton;
    }

    public WebElement getAddNewGoalButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                , addNewGoalButton);
        return addNewGoalButton;
    }

    public WebElement getEngagementGoalItem() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                , engagementGoalItem);
        return engagementGoalItem;
    }

    public WebElement getSelectDropDownGoalList() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                , selectDropDownGoalList);
        return selectDropDownGoalList;
    }

    public WebElement getAlertCloseButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                , alertCloseButton);
        return alertCloseButton;
    }
}
