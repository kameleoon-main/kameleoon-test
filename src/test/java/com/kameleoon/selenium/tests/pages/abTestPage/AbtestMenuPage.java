package com.kameleoon.selenium.tests.pages.abTestPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 14.03.16.
 *
 */
public class AbtestMenuPage extends BasePage {
    public AbtestMenuPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return abtestMenu.isDisplayed();
    }

    //-- AB Test Menu --//
    @FindBy(xpath = ".//*[contains(@class, 'abtest-menu')]")
    private WebElement abtestMenu;

    @FindBys(@FindBy(xpath = ".//*[contains(@class, 'abtest-menu')]//*[contains(@class, 'button')]"))
    private List<WebElement> abtestMenuList;
    //-----------------//


    public List<WebElement> getAbtestMenuList(){
        return abtestMenuList;
    }
}
