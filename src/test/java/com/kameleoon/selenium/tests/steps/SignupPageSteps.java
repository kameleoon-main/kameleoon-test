package com.kameleoon.selenium.tests.steps;

import com.kameleoon.selenium.tests.pages.SignupPage;

import java.util.Random;
import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 03.03.16.
 *
 */
public class SignupPageSteps {

    private SignupPage signupPage;

    //public static String company;
    public static String firstName;
    public static String lastName;
    public static String email;
    public static String password;
    public static String telephone;
    public static String url;

    public SignupPageSteps() {
        signupPage = new SignupPage();
        int randomInt = createRandomInt();
        //company = randomInt + "Company";
        firstName = randomInt + "FirstName";
        lastName = randomInt + "LastName";
        email = randomInt + "@test.com";
        password = "Qwerty11";
        telephone = String.valueOf(randomInt);
        url = "http://gallery-belyaevo.ru";
    }

    public WelcomePageSteps createNewAccount(){
        //signupPage.getCompanyInputText().sendKeys(company);
        signupPage.getFirstnameInputText().sendKeys(firstName);
        signupPage.getEmailInputText().sendKeys(email);
        signupPage.getTelephoneInputText().sendKeys(telephone);
        signupPage.getLastnameInputText().sendKeys(lastName);
        signupPage.getPasswordInputText().sendKeys(password);
        signupPage.getUrlInputText().sendKeys(url);
        signupPage.getSignupButton().click();
        return page(WelcomePageSteps.class);
    }


    private static int createRandomInt(){
        Random rand = new Random();
        int max = 10000;
        int min =0;
        return rand.nextInt((max - min) + 1) + min;
    }
}
