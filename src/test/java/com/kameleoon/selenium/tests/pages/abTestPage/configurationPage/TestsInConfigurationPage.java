package com.kameleoon.selenium.tests.pages.abTestPage.configurationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 01.03.16.
 *
 */
public class TestsInConfigurationPage extends BasePage {
    public TestsInConfigurationPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'alpha')]/h2")
    private WebElement titleActionText;

    //-- Launch Editor --//
    @FindBy(xpath = ".//table//tr[2]//*[contains(@class, 'relative')]")
    private WebElement launchEditorButton;
    //------------------//

    //-- Menu in Table for the first element --//
    @FindBy(xpath = ".//table//tr[2]//*[contains(@class, 'settings')]")
    private WebElement menuSettings;

    @FindBy(xpath = ".//table//tr[2]//*[contains(@class, 'delete')]")
    private WebElement deleteButton;

    @FindBy(xpath = ".//table//tr[2]//li[contains(@class, 'edit')]")
    private WebElement editUrlButton;

    @FindBy(xpath = ".//table//tr[2]//li[contains(@class, 'submenu')]")
    private WebElement copySubmenu;

    @FindBy(xpath = ".//table//tr[2]//li[contains(@class, 'copy')]")
    private WebElement copyTestButton;
    //-----------------------------------------//

    //-- Rows --//
    @FindBys(@FindBy(xpath = ".//table//*[contains(@class, 'table-row')]"))
    private List<WebElement> rowsTable;

    @FindBy(xpath = ".//table//tr[2]//td[3]//*[contains(@class, 'ellipsed')]")
    private WebElement linkedURLText;
    //----------//


    public WebElement getTitleActionText(){
        return titleActionText;
    }

    public WebElement getMenuSettings(){
        return menuSettings;
    }

    public WebElement getDeleteButton(){
        return deleteButton;
    }

    public List<WebElement> getRowsTable(){
        return rowsTable;
    }

    public WebElement getEditUrlButton () {
        return editUrlButton;
    }

    public WebElement getLinkedURLText() {
        return linkedURLText;
    }

    public WebElement getCopySubmenu(){
        return copySubmenu;
    }

    public WebElement getCopyTestButton(){
        return copyTestButton;
    }

    public WebElement getLaunchEditorButton() {
        return launchEditorButton;
    }
}
