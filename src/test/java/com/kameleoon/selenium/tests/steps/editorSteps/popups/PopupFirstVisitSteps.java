package com.kameleoon.selenium.tests.steps.editorSteps.popups;

import com.kameleoon.selenium.tests.pages.editorPage.popups.PopupFirstVisitPage;
import com.kameleoon.selenium.tests.steps.editorSteps.EditorMainSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 17.03.16.
 *
 */
public class PopupFirstVisitSteps {
    private PopupFirstVisitPage popupRegistrConfirmPage;
    public PopupFirstVisitSteps() {
        popupRegistrConfirmPage = new PopupFirstVisitPage();
    }

    public EditorMainSteps closePopup(){
        popupRegistrConfirmPage.getInterruptVisitButton().click();
        return page(EditorMainSteps.class);
    }
}
