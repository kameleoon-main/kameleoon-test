package com.kameleoon.selenium.tests.steps.globalHeaderSteps;

import com.kameleoon.selenium.tests.pages.globalHeaderPage.GlobalHeaderPage;
import com.kameleoon.selenium.tests.steps.ExperimentsPageSteps;
import com.kameleoon.selenium.tests.steps.LoginPageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.steps.settingsSteps.PreferencesSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 01.03.16.
 *
 */
public class GlobalHeaderSteps {

    private GlobalHeaderPage globalHeaderPage;

    public GlobalHeaderSteps(){
        globalHeaderPage = new GlobalHeaderPage();
    }

    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public ExperimentsPageSteps installScriptButtonClick(){
        globalHeaderPage.getInstallScriptButton().click();
        return new ExperimentsPageSteps();
    }

    public void closeInstallationDialog(){
        globalHeaderPage.getCloseInstallationDialogButton().click();
    }

    public Boolean getInstallationDialogVisible(){
        return globalHeaderPage.getCloseInstallationDialogButton().isDisplayed();
    }

    public LoginPageSteps logoutClick(){
        moveMouseToElement(globalHeaderPage.getUserMenu());
        globalHeaderPage.getLogoutButton().click();
        return page(LoginPageSteps.class);
    }

    public PopupNewExperimentPageSteps newTestButtonClick(){
        globalHeaderPage.getNewTestButton().click();
        return page(PopupNewExperimentPageSteps.class);
    }

    public PreferencesSteps goToSettings(){
        moveMouseToElement(globalHeaderPage.getSettingsMenu());
        globalHeaderPage.getSettingsButton().click();
        return page(PreferencesSteps.class);
    }


}
