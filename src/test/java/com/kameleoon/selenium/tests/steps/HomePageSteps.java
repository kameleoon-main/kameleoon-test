package com.kameleoon.selenium.tests.steps;

import com.kameleoon.selenium.tests.pages.HomePage;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 04.03.16.
 *
 */
public class HomePageSteps {

    private HomePage homePage;

    public HomePageSteps(){
        homePage = new HomePage();
    }

    public ArrayList<String> getListContentHeaderText(){
        ArrayList<String> listString = new ArrayList<>();
        List<WebElement> listWeb = homePage.getMyContentHeaderText();
        for (WebElement element:listWeb) {
            listString.add(element.getText());
        }
        return listString;
    }

    public TestsInConfigurationSteps clickCreateTest(){
        homePage.getCreateTestButton().click();
        return page(TestsInConfigurationSteps.class);
    }

    public UpdatePersonalizationSteps clickCreatePersonalizations(){
        homePage.getCreatePersonalizationButton().click();
        return page(UpdatePersonalizationSteps.class);
    }


}
