package com.kameleoon.selenium.tests.utils;

import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.math.Rational;

import java.awt.*;
import java.io.File;

import static org.monte.media.FormatKeys.*;
import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.VideoFormatKeys.*;

/**
 * Created by roman on 29.02.16.
 *
 */
public class ScreenRecorder {
    private static org.monte.screenrecorder.ScreenRecorder screenRecorder;
    private static String RECORD_DIRECTORY = "./target/screenrecorder/";
    public static Boolean REMOVE_VIDEO_SUCCESSFUL_TEST = true;

    public static void beforeTest() {
        try {
            if (screenRecorder == null) {
                //Create a instance of GraphicsConfiguration to get the Graphics configuration
                //of the Screen. This is needed for ScreenRecorder class.
                GraphicsConfiguration gc = GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration();

                File dir = new File(RECORD_DIRECTORY);

                //Create a instance of ScreenRecorder with the required configurations
                screenRecorder = new org.monte.screenrecorder.ScreenRecorder(gc, null,
                new Format(MediaTypeKey, FormatKeys.MediaType.FILE, MimeTypeKey, MIME_AVI),
                new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                        CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                        DepthKey, (int)24, FrameRateKey, Rational.valueOf(15),
                        QualityKey, 1.0f,
                        KeyFrameIntervalKey, (int) (15 * 60)),
                new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey,
                        "black", FrameRateKey, Rational.valueOf(30)),
                null, dir);
            }

            if (!screenRecorder.getState().equals(org.monte.screenrecorder.ScreenRecorder.State.RECORDING)) {
                screenRecorder.start();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static void afterTest() {
        try {
            screenRecorder.stop();
        }
        catch (Exception e) {
            System.out.println(e);
        }
        java.util.List<File> createdMovieFiles = screenRecorder.getCreatedMovieFiles();
        for(File movie : createdMovieFiles){
            System.out.println("New video files created: " + movie.getAbsolutePath());
        }

        // If the test fails don't call removeVideoFiles
        if (REMOVE_VIDEO_SUCCESSFUL_TEST) {
            removeVideoFiles();
        }
    }

    private static void removeVideoFiles() {
        java.util.List<File> files = screenRecorder.getCreatedMovieFiles();
        for (File movie : files) {
            movie.delete();
            System.out.println("Removing video files: " + movie.getAbsolutePath());
        }
    }

}
