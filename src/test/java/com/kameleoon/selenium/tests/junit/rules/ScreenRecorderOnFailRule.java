package com.kameleoon.selenium.tests.junit.rules;

import com.kameleoon.selenium.tests.utils.ScreenRecorder;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import static com.kameleoon.selenium.tests.utils.ScreenRecorder.*;

/**
 * Created by rrog on 29.02.2016.
 * This class represent rule - junit mechanism for adding awesome functionality in test running process.
 * This rule adds screenRecorder taking when test fails
 */
public class ScreenRecorderOnFailRule implements TestRule {

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                beforeTest();
                try {
                    base.evaluate();
                }
                catch (Throwable t) {
                    ScreenRecorder.REMOVE_VIDEO_SUCCESSFUL_TEST = false;
                    throw t;
                }
                finally {
                    afterTest();
                }
            }
        };
    }

}
