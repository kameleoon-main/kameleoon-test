package com.kameleoon.selenium.tests.steps;

import com.kameleoon.selenium.tests.pages.ExperimentsPage;

/**
 * Created by roman on 03.03.16.
 *
 */
public class ExperimentsPageSteps {

    private ExperimentsPage experimentsPage;

    public ExperimentsPageSteps() {
        experimentsPage = new ExperimentsPage();
    }

    public String getActiveButtonText(){
        return experimentsPage.getActiveButtonText().getText();
    }
}
