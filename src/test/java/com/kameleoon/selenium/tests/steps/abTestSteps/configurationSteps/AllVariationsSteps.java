package com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps;

import com.kameleoon.selenium.tests.pages.abTestPage.configurationPage.AllVariationsPage;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmCopySteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 14.03.16.
 */
public class AllVariationsSteps {
    private AllVariationsPage allVariationsPage;
    public AllVariationsSteps() {
        allVariationsPage = new AllVariationsPage();
    }

    public String getTitleActionText(){
        return allVariationsPage.getTitleActionText().getText();
    }

    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    public int countRowsInAllVariations() {
        int count = allVariationsPage.getRowsTable().size();
        return count;
    }

    public PopupConfirmCopySteps copyTest(String site, String test){
        moveMouseToElement(allVariationsPage.getMenuSettings());
        List<WebElement> listSite = allVariationsPage.getCopySubmenu();
        for (WebElement element:listSite) {
            if (element.getText().contains(site))
                moveMouseToElement(element);
        }
        List<WebElement> listTest = allVariationsPage.getCopyTestButton();
        for (WebElement element:listTest) {
            if(element.getText().contains(test))
                element.click();
        }
        return page(PopupConfirmCopySteps.class);
    }

}
