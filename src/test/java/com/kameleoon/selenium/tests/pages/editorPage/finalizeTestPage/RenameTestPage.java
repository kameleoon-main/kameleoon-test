package com.kameleoon.selenium.tests.pages.editorPage.finalizeTestPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 25.03.16.
 *
 */
public class RenameTestPage extends BasePage {
    public RenameTestPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return startTestButton.isDisplayed();
    }

    @FindBy(xpath = "(.//*[@id='kameleoonQxPopIn']//*[contains(@qxclass,'qx.ui.form.Button')])[2]")
    private WebElement startTestButton;


    public WebElement getStartTestButton() {
        return startTestButton;
    }
}
