package com.kameleoon.selenium.tests.steps.abTestSteps.popups;

import com.kameleoon.selenium.tests.pages.abTestPage.popups.PopupConfirmCopyPage;

/**
 * Created by roman on 14.03.16.
 */
public class PopupConfirmCopySteps {

    private PopupConfirmCopyPage popupConfirmCopyPage;

    public PopupConfirmCopySteps() {
        popupConfirmCopyPage = new PopupConfirmCopyPage();
    }

    public void clckAgree(){
        popupConfirmCopyPage.getOkButton().click();
    }
}
