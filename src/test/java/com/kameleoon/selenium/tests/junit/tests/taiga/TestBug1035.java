package com.kameleoon.selenium.tests.junit.tests.taiga;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.AbtestMenuSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.AllVariationsSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmCopySteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.newLogin;
import static com.kameleoon.selenium.tests.helpers.settings.CreateNewSite.addNewSite;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by roman on 24.03.16.
 *
 */
public class TestBug1035 {

    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void test(){
        newLogin();
        //login();
        addNewSite("http://belvidoor.ru");

        /** Go to all variations AB test */
        new HomePageSteps().clickCreateTest();
        AllVariationsSteps allVariationsSteps = new AbtestMenuSteps().goToAllVariations();
        int countRows = allVariationsSteps.countRowsInAllVariations();

        /** Copy AB test */
        PopupConfirmCopySteps popupConfirmCopySteps = allVariationsSteps.copyTest("belvidoor.ru","Test 1");
        popupConfirmCopySteps.clckAgree();

        /** Check that copy AB test is exist in table */
        assertThat(allVariationsSteps.countRowsInAllVariations(), equalTo(countRows + 1));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
