package com.kameleoon.selenium.tests.steps.settingsSteps;

import com.kameleoon.selenium.tests.pages.settingsPage.PreferencesPage;
import com.kameleoon.selenium.tests.steps.settingsSteps.configurationSteps.ConfigurationSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import org.openqa.selenium.WebElement;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 23.03.16.
 *
 */
public class PreferencesSteps {
    private PreferencesPage preferencesPage;
    public PreferencesSteps() {
        preferencesPage = new PreferencesPage();
    }

    public ConfigurationSteps clickConfiguration(){
        WebElement configuration = preferencesPage.getConfigurationButtons().get(preferencesPage.getConfigurationButtons().size() - 1);
        configuration.click();
        TimeUtils.waitForSeconds(1);
        return page(ConfigurationSteps.class);
    }

    public void clickAddNewSite(){
        preferencesPage.getNewSiteButton().click();
        TimeUtils.waitForSeconds(1);
    }

}
