package com.kameleoon.selenium.tests.pages.personalizationPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 22.03.16.
 *
 */
public class PopupSaveConfirmPage extends BasePage {
    public PopupSaveConfirmPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return saveConfirmOkButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='registr_confirm']//*[contains(@class,'bleu')]")
    private WebElement saveConfirmOkButton;

    public WebElement getSaveConfirmOkButton() {
        return saveConfirmOkButton;
    }
}
