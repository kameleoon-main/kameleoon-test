package com.kameleoon.selenium.tests.pages.globalHeaderPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 09.03.16.
 *
 */
public class GlobalHeaderPage extends BasePage {

    public GlobalHeaderPage(){super();}

    @Override
    public boolean isPageOpened() {
        return userMenu.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'installation_dialog_button')]")
    private WebElement installScriptButton;

    @FindBy(xpath = ".//*[contains(@class, 'exit')]")
    private WebElement closeInstallationDialogButton;

    //-- User Menu --//
    @FindBy(xpath = ".//*[contains(@class, 'user-menu')]")
    private WebElement userMenu;

    @FindBy(xpath = ".//*[contains(@href, 'logout')]")
    private WebElement logoutButton;
    //--------------//

    //-- Settings Menu --//
    @FindBy(xpath = ".//a[contains(@class, 'kameleoon-preference')]")
    private WebElement settingsMenu;

    @FindBy(xpath = "(.//*[@class='preference-element'])[1]")
    private WebElement settingsButton;
    //------------------//

    @FindBy(xpath = ".//*[@id='newExperimentButton']")
    private WebElement newTestButton;




    public WebElement getInstallScriptButton(){
        return installScriptButton;
    }

    public WebElement getCloseInstallationDialogButton(){
        return closeInstallationDialogButton;
    }

    public WebElement getLogoutButton(){
        return logoutButton;
    }

    public WebElement getUserMenu() {
        return userMenu;
    }

    public WebElement getNewTestButton() {
        return newTestButton;
    }

    public WebElement getSettingsMenu() {
        return settingsMenu;
    }

    public WebElement getSettingsButton() {
        return settingsButton;
    }
}
