package com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps;

import com.kameleoon.selenium.tests.pages.abTestPage.configurationPage.TestsInConfigurationPage;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmCopySteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmDeleteSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupUpdateUrlSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Set;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 10.03.16.
 *
 */
public class TestsInConfigurationSteps {

    private TestsInConfigurationPage testsInConfigurationPage;

    public TestsInConfigurationSteps(){
        testsInConfigurationPage = new TestsInConfigurationPage();
    }


    private void moveMouseToElement(WebElement element){
        Actions actions = new Actions(WebDriverFactory.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }


    public String getTitleActionText(){
        return testsInConfigurationPage.getTitleActionText().getText();
    }

    public String getLinkedURLText() {
        return testsInConfigurationPage.getLinkedURLText().getText();
    }

    public PopupConfirmDeleteSteps deleteABTest(){
        moveMouseToElement(testsInConfigurationPage.getMenuSettings());
        testsInConfigurationPage.getDeleteButton().click();
        return page(PopupConfirmDeleteSteps.class);
    }

    public PopupUpdateUrlSteps editUrl(){
        moveMouseToElement(testsInConfigurationPage.getMenuSettings());
        testsInConfigurationPage.getEditUrlButton().click();
        return page(PopupUpdateUrlSteps.class);
    }

    public PopupConfirmCopySteps copyTest(){
        moveMouseToElement(testsInConfigurationPage.getMenuSettings());
        moveMouseToElement(testsInConfigurationPage.getCopySubmenu());
        testsInConfigurationPage.getCopyTestButton().click();
        return page(PopupConfirmCopySteps.class);
    }

    public void goToEditorPage(){
        /* get the set of handles currently open windows */
        Set<String> oldWindowsSet = WebDriverFactory.getDriver().getWindowHandles();
        /* go to site (open new window) and wait 5 seconds */
        testsInConfigurationPage.getLaunchEditorButton().click();
        TimeUtils.waitForSeconds(5);
        /* get the new set of handles */
        Set<String> newWindowsSet = WebDriverFactory.getDriver().getWindowHandles();
        /* get new window */
        newWindowsSet.removeAll(oldWindowsSet);
        String newWindowHandle = newWindowsSet.iterator().next();
        /* pass control to new window */
        WebDriverFactory.getDriver().switchTo().window(newWindowHandle);
        WebDriverFactory.getDriver().switchTo().activeElement();
    }


    public int countRowsInTestsConfiguration() {
        return testsInConfigurationPage.getRowsTable().size();
    }


}
