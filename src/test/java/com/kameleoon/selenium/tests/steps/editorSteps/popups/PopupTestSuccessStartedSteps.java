package com.kameleoon.selenium.tests.steps.editorSteps.popups;

import com.kameleoon.selenium.tests.pages.editorPage.popups.PopupTestSuccessStartedPage;

/**
 * Created by roman on 25.03.16.
 *
 */
public class PopupTestSuccessStartedSteps {
    private PopupTestSuccessStartedPage popupTestSuccessStartedPage;
    public PopupTestSuccessStartedSteps() {
        popupTestSuccessStartedPage = new PopupTestSuccessStartedPage();
    }

    public void clickOkButton(){
        popupTestSuccessStartedPage.getOkButton().click();
    }
}
