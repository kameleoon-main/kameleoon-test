package com.kameleoon.selenium.tests.junit.tests.abtest.testsInConfiguration;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupConfirmDeleteSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by roman on 10.03.16.
 */
public class DeleteABTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testDeleteABTest(){
        login();
        //Create new AB test
        TestsInConfigurationSteps testsInConfigurationSteps = new HomePageSteps().clickCreateTest();
        int countRows = testsInConfigurationSteps.countRowsInTestsConfiguration();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = new GlobalHeaderSteps().newTestButtonClick();
        testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-DeleteABTest");
        //Verify that new AB test is exist in table
        assertThat(testsInConfigurationSteps.countRowsInTestsConfiguration(), equalTo(countRows + 1));

        //Remove this AB test
        PopupConfirmDeleteSteps popupConfirmDeleteSteps = testsInConfigurationSteps.deleteABTest();
        popupConfirmDeleteSteps.clckAgree();
        //Verify that this AB test is not exist in table
        assertThat(testsInConfigurationSteps.countRowsInTestsConfiguration(), equalTo(countRows));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
