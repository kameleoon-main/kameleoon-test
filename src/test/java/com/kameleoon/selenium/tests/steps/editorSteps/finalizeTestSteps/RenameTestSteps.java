package com.kameleoon.selenium.tests.steps.editorSteps.finalizeTestSteps;

import com.kameleoon.selenium.tests.pages.editorPage.finalizeTestPage.RenameTestPage;
import com.kameleoon.selenium.tests.steps.editorSteps.popups.PopupTestSuccessStartedSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 25.03.16.
 *
 */
public class RenameTestSteps {
    private RenameTestPage renameTestPage;
    public RenameTestSteps() {
        renameTestPage = new RenameTestPage();
    }

    public PopupTestSuccessStartedSteps clickStartTestButton(){
        renameTestPage.getStartTestButton().click();
        return page(PopupTestSuccessStartedSteps.class);
    }

}
