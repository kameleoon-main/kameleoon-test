package com.kameleoon.selenium.tests.pages.editorPage.finalizeTestPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 25.03.16.
 *
 */
public class FinalizeTestPage extends BasePage {
    public FinalizeTestPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return validateSettingsButton.isDisplayed();
    }

    @FindBy(xpath = "(.//*[@id='kameleoonQxPopIn']//*[@class='qx-UDPopInFrame']" +
            "/*[contains(@qxclass,'qx.ui.container.Composite')]" +
            "/*[contains(@qxclass,'qx.ui.form.Button')])[3]/*[contains(@qxclass,'qx.ui.basic.Label')]")
    private WebElement validateSettingsButton;

    @FindBys(@FindBy(xpath = ".//*[@id='kameleoonQxPopIn']//*[contains(@class,'qx-UDSimpleZone')]" +
            "//*[contains(@class,'qx-UDMenuListItem')]//*[contains(@qxclass,'qx.ui.basic.Image')]"))
    private List<WebElement> chooseVariationsItem;

    @FindBy(xpath = ".//*[@id='kameleoonQxPopIn']//*[contains(@class,'qx-UDSimpleZone')]" +
            "//*[contains(@qxclass,'qx.ui.form.CheckBox')]//*[contains(@qxclass,'qx.ui.basic.Image')]")
    private WebElement chooseReportingCheckBox;

    @FindBys(@FindBy(xpath = ".//*[@id='kameleoonQxPopIn']//*[contains(@class,'qx-UDSimpleZone')]" +
            "//*[contains(@class,'qx-UDMenuListItem')]//*[contains(@qxclass,'qx.ui.basic.Image')]"))
    private List<WebElement> chooseGoalsItem;



    public WebElement getValidateSettingsButton() {
        return validateSettingsButton;
    }

    public List<WebElement> getChooseVariationsItem() {
        return chooseVariationsItem;
    }

    public WebElement getChooseReportingCheckBox() {
        return chooseReportingCheckBox;
    }

    public List<WebElement> getChooseGoalsItem() {
        return chooseGoalsItem;
    }
}
