package com.kameleoon.selenium.tests.pages.editorPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 17.03.16.
 *
 */
public class PopupFirstVisitPage extends BasePage {
    public PopupFirstVisitPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return interruptVisitButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='kameleoonQooxdooRoot']/div[5]/div[5]")
    private WebElement interruptVisitButton;

    public WebElement getInterruptVisitButton() {
        return interruptVisitButton;
    }
}
