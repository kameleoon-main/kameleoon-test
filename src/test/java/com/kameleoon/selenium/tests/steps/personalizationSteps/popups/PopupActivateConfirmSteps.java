package com.kameleoon.selenium.tests.steps.personalizationSteps.popups;

import com.kameleoon.selenium.tests.pages.personalizationPage.popups.PopupActivateConfirmPage;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 14.03.16.
 *
 */
public class PopupActivateConfirmSteps {
    private PopupActivateConfirmPage popupActivateConfirmPage;
    public PopupActivateConfirmSteps() {
        popupActivateConfirmPage = new PopupActivateConfirmPage();
    }

    public UpdatePersonalizationSteps clickOk(){
        popupActivateConfirmPage.getActivateConfirmOkButton().click();
        return page(UpdatePersonalizationSteps.class);
    }
}
