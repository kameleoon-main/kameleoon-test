package com.kameleoon.selenium.tests.pages.widgetsPage.countdownBanner;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 21.03.16.
 *
 */
public class InsideCountdownBannerPage extends BasePage {
    public InsideCountdownBannerPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getBackToWidgetsButton().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class,'add_timer-inside')]//*[@class='back_position']")
    private WebElement backToWidgetsButton;


    public WebElement getBackToWidgetsButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",backToWidgetsButton);
        return backToWidgetsButton;
    }


}
