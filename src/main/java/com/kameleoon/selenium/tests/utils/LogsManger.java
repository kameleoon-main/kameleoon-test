package com.kameleoon.selenium.tests.utils;

import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.Logs;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by roman on 28.03.16.
 *
 */
public class LogsManger {

    public static ArrayList<String> getBrowserLogException() {
        ArrayList<String> exceptionList = new ArrayList<>();
        Logs log = WebDriverFactory.getDriver().manage().logs();
        List<LogEntry> logsEntries = log.get("browser").filter(Level.SEVERE);
        for (LogEntry entry : logsEntries) {
            exceptionList.add(entry.getMessage());
//            System.out.println(entry.getMessage());
        }
        return exceptionList;
    }

}
