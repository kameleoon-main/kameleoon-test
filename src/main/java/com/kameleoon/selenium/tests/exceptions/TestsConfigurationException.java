package com.kameleoon.selenium.tests.exceptions;

/**
 * Created by rrog on 16.01.2016.
 */
public class TestsConfigurationException extends RuntimeException {

    public TestsConfigurationException(String message) {
        super(message);
    }

    public TestsConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
