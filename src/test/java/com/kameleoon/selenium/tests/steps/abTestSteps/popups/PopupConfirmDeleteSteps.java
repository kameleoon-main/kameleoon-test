package com.kameleoon.selenium.tests.steps.abTestSteps.popups;

import com.kameleoon.selenium.tests.pages.abTestPage.popups.PopupConfirmDeletePage;

/**
 * Created by roman on 10.03.16.
 */
public class PopupConfirmDeleteSteps {
    PopupConfirmDeletePage popupConfirmDeletePage;

    public PopupConfirmDeleteSteps() {
        popupConfirmDeletePage = new PopupConfirmDeletePage();
    }

    public void clckAgree(){
        popupConfirmDeletePage.getOkButton().click();
    }
}
