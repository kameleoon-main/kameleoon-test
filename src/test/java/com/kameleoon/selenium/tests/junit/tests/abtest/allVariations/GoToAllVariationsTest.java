package com.kameleoon.selenium.tests.junit.tests.abtest.allVariations;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.AbtestMenuSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.AllVariationsSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by roman on 14.03.16.
 */
public class GoToAllVariationsTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }

    @Test
    public void testGoToAllVariationsTests(){
        login();
        new HomePageSteps().clickCreateTest();
        AllVariationsSteps allVariationsSteps= new AbtestMenuSteps().goToAllVariations();
        assertThat(allVariationsSteps.getTitleActionText(), equalTo("Toutes les variantes"));
    }



    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
