package com.kameleoon.selenium.tests.steps.personalizationSteps.segmentBuilderSteps.segmentDefinition;

import com.kameleoon.selenium.tests.pages.personalizationPage.segmentBuilderPage.segmentDefinition.PageURLPage;

/**
 * Created by roman on 18.03.16.
 *
 */
public class PageURLSteps {
    private PageURLPage pageURLPage;
    public PageURLSteps() {
        pageURLPage = new PageURLPage();
    }

    public void inputFirstValue(String text){
        pageURLPage.getFirstValueInputText().sendKeys(text);
    }

}
