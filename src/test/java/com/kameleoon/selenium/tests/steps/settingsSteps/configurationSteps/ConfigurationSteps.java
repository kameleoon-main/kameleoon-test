package com.kameleoon.selenium.tests.steps.settingsSteps.configurationSteps;

import com.kameleoon.selenium.tests.pages.settingsPage.configurationPage.ConfigurationPage;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 23.03.16.
 *
 */
public class ConfigurationSteps {
    private ConfigurationPage configurationPage;
    public ConfigurationSteps() {
        configurationPage = new ConfigurationPage();
    }

    public ConfigurationSteps createNewUrl(String url){
        configurationPage.getUrlInputText().clear();
        configurationPage.getUrlInputText().sendKeys(url);
        configurationPage.getValidateButton().click();
        return page(ConfigurationSteps.class);
    }
}
