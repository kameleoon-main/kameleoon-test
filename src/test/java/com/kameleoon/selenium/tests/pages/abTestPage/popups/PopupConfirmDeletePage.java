package com.kameleoon.selenium.tests.pages.abTestPage.popups;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 10.03.16.
 *
 */
public class PopupConfirmDeletePage extends BasePage {
    public PopupConfirmDeletePage() {
        super();
    }

    @FindBy(xpath = ".//*[@id='confirm-delete-popup']//*[contains(@class, 'name')]")
    private WebElement abTestNameText;

    @FindBy(xpath = ".//*[@id='confirm-delete-popup']//*[contains(@class, 'cancel')]")
    private WebElement cancelButton;

    @FindBy(xpath = ".//*[@id='confirm-delete-popup']//*[contains(@class, 'green')]")
    private WebElement okButton;

    @Override
    public boolean isPageOpened() {
        return abTestNameText.isDisplayed();
    }

    public WebElement getAbTestNameText() {
        return abTestNameText;
    }

    public WebElement getCancelButton() {
        return cancelButton;
    }

    public WebElement getOkButton() {
        return okButton;
    }
}
