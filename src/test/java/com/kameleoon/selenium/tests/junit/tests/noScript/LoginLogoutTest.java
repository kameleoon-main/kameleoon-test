package com.kameleoon.selenium.tests.junit.tests.noScript;

import com.kameleoon.selenium.tests.configuration.properties.Properties;
import org.junit.*;
import com.kameleoon.selenium.tests.junit.rules.ScreenRecorderOnFailRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.LoginPageSteps;
import com.kameleoon.selenium.tests.steps.WelcomePageSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.open;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by rrog on 16.01.2016.
 *
 */
public class LoginLogoutTest {
    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @Rule
    public ScreenRecorderOnFailRule screenRecorderOnFailRule = new ScreenRecorderOnFailRule();


    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }

    @Test
    public void testLogin() {
        /** Login */
        WelcomePageSteps welcomePageSteps = open(Properties.getUrl(), LoginPageSteps.class)
                .loginAs("test", "development", WelcomePageSteps.class);
        /** Verify user is loginAs */
        assertThat(welcomePageSteps.getWelcomeBarText(), equalTo("Bienvenue"));
        /** Go to AB test page and  close installation dialog*/
        GlobalHeaderSteps globalHeaderSteps = welcomePageSteps.clickABtestButton();
        globalHeaderSteps.closeInstallationDialog();
        /** Verify that Installation Dialog is not visible */
        assertThat(globalHeaderSteps.getInstallationDialogVisible(), equalTo(false));
        /** Logout and Verify user is logout */
        assertThat(globalHeaderSteps.logoutClick().getInvalidMessageText(), equalTo("Vous avez été déconnecté."));
    }

    @Test
    public void testNegativeLogin(){
        //Verify user is logout
        assertNotNull(open(Properties.getUrl(), LoginPageSteps.class)
                .loginAs("test", "test", LoginPageSteps.class)
                .getErrorMessageText()
        );
    }


    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
