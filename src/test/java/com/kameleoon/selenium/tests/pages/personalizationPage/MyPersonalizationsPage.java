package com.kameleoon.selenium.tests.pages.personalizationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 15.03.16.
 *
 */
public class MyPersonalizationsPage extends BasePage {
    public MyPersonalizationsPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return titleActionText.isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class, 'block_title')]")
    private WebElement titleActionText;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'tiles__header')]"))
    private List<WebElement> titlesHeaderText;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'show_edit')]"))
    private List<WebElement> editPersoButtons;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'tiles_config_name')]"))
    private List<WebElement> resultPersoButtons;

    //-- Duplicat Personalizations --//
    @FindBys(@FindBy(xpath = ".//*[contains(@class,'tiles_actions_wrapper')]/span[2]"))
    private List<WebElement> duplicatePersoButtons;

    @FindBys(@FindBy(xpath = ".//*[@class='select_option']/*[@class='select_option_btn']"))
    private List<WebElement> selectOptionButtons;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'select_option_active')]//*[@class='select_option_item']"))
    private List<WebElement> selectSiteItem;

    @FindBys(@FindBy(xpath = ".//*[contains(@class,'btn_ok')]"))
    private List<WebElement> okDuplicateButtons;
    //------------------------------//


    public WebElement getTitleActionText() {
        return titleActionText;
    }

    public List<WebElement> getTitlesHeaderText() {
        return titlesHeaderText;
    }

    public List<WebElement> getEditPersoButtons() {
        return editPersoButtons;
    }

    public List<WebElement> getResultPersoButtons() {
        return resultPersoButtons;
    }

    public List<WebElement> getDuplicatePersoButtons() {
        return duplicatePersoButtons;
    }

    public List<WebElement> getSelectOptionButtons() {
        return selectOptionButtons;
    }

    public List<WebElement> getSelectSiteItem() {
        return selectSiteItem;
    }

    public List<WebElement> getOkDuplicateButtons() {
        return okDuplicateButtons;
    }
}
