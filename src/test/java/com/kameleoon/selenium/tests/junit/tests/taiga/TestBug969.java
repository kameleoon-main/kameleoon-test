package com.kameleoon.selenium.tests.junit.tests.taiga;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.popups.PopupNewExperimentPageSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.EditorMainSteps;
import com.kameleoon.selenium.tests.steps.editorSteps.popups.PopupFirstVisitSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.GlobalHeaderSteps;
import com.kameleoon.selenium.tests.steps.widgetsSteps.WidgetsSteps;
import com.kameleoon.selenium.tests.steps.widgetsSteps.countdownBanner.AboveCountdownBannerSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.newLogin;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static com.kameleoon.selenium.tests.helpers.JavaScript.launchEditor;

/**
 * Created by roman on 21.03.16.
 *
 */
public class TestBug969 {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void test(){
        newLogin();

        /**Create new AB test */
        new HomePageSteps().clickCreateTest();
        PopupNewExperimentPageSteps popupNewExperimentPageSteps = new GlobalHeaderSteps().newTestButtonClick();
        TestsInConfigurationSteps testsInConfigurationSteps = popupNewExperimentPageSteps.createNewTest("test-CreateNewABTest");

        /** Go to Editor and.. */
        testsInConfigurationSteps.goToEditorPage();
        /** If we expect PopupFirstVisit page */
        PopupFirstVisitSteps popupFirstVisitSteps = launchEditor(PopupFirstVisitSteps.class);
        EditorMainSteps editorMainSteps = popupFirstVisitSteps.closePopup();
        /** If we expect EditorMain page */
        //EditorMainSteps editorMainSteps = launchEditor(EditorMainSteps.class);

        /** Variation menu */
        editorMainSteps.clickVariationMenu();

        /** Go to Widgets */
        WidgetsSteps widgetsSteps = editorMainSteps.clickAddWidgetButton(WidgetsSteps.class);
        AboveCountdownBannerSteps aboveCountdownBannerSteps =  widgetsSteps.addTimerAbove();
        aboveCountdownBannerSteps.clickHorizontalAlignment();
        aboveCountdownBannerSteps.saveWidgetAndGoToEditor();

        /** Return to page AboveCountdownBanner */
        editorMainSteps = launchEditor(EditorMainSteps.class);
        editorMainSteps.clickVariationMenu();
        aboveCountdownBannerSteps = editorMainSteps.clickAddWidgetButton(AboveCountdownBannerSteps.class);

        /** Check that the HorizontalAlignment was saved */
        assertThat(aboveCountdownBannerSteps.getCheckedCheckbox("Horizontal alignment"), is(true));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
