package com.kameleoon.selenium.tests.steps.abTestSteps.popups;

import com.kameleoon.selenium.tests.pages.abTestPage.popups.PopupNewExperimentPage;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import org.openqa.selenium.Keys;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 09.03.16.
 */
public class PopupNewExperimentPageSteps {

    private PopupNewExperimentPage popupNewExperimentPage;

    public PopupNewExperimentPageSteps() {
        popupNewExperimentPage = new PopupNewExperimentPage();
    }

    public TestsInConfigurationSteps createNewTest(String name){
        //popupNewExperimentPage.getNameTestInputText().click();
        popupNewExperimentPage.getNameTestInputText().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        popupNewExperimentPage.getNameTestInputText().sendKeys(Keys.BACK_SPACE);
        popupNewExperimentPage.getNameTestInputText().sendKeys(name);
        popupNewExperimentPage.getOkButton().click();
        return page(TestsInConfigurationSteps.class);
    }

}
