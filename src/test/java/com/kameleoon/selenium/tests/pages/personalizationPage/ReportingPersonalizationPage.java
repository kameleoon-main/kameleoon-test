package com.kameleoon.selenium.tests.pages.personalizationPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 18.03.16.
 *
 */
public class ReportingPersonalizationPage extends BasePage {
    public ReportingPersonalizationPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getAddGoalButton().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class,'add-goal-button')]")
    private WebElement addGoalButton;

    //-- Kameleoon menu item --//
    @FindBy(xpath = ".//*[@id='goal-form']//input[contains(@class,'goal-name')]")
    private WebElement goalNameInputText;

    @FindBy(xpath = ".//*[@id='goal-form']//*[contains(@class,'goal-url')]")
    private WebElement goalUrlInputText;
    //-------------------------//

    @FindBy(xpath = ".//*[@id='goal-form']//*[contains(@class,'button_green')]")
    private WebElement validateButton;

    public WebElement getAddGoalButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,addGoalButton);
        return addGoalButton;
    }

    public WebElement getGoalNameInputText() {
        return goalNameInputText;
    }

    public WebElement getValidateButton() {
        return validateButton;
    }

    public WebElement getGoalUrlInputText() {
        return goalUrlInputText;
    }
}
