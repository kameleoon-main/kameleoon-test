package com.kameleoon.selenium.tests.junit.rules;


import org.junit.ClassRule;
import org.junit.rules.ExternalResource;
import com.kameleoon.selenium.tests.utils.DBConnectionManager;


/**
 * Created by roman on 03.03.16.
 * This class represent rule - junit mechanism for adding awesome functionality in test running process.
 * This rule of opening a connection and closing a connection for entire TestClass
 */

//@RunWith(Suite.class)
//@Suite.SuiteClasses({CreateNewAccountTest.class})
public class DBConnectionRule extends ExternalResource {
    @ClassRule
    public static DBConnectionManager dbConnectionManager = new DBConnectionManager();

    @Override
    protected void before() throws Throwable {
        dbConnectionManager.open();
    }

    @Override
    protected void after() {
        dbConnectionManager.close();
    }
}



