package com.kameleoon.selenium.tests.junit.tests.personalization;

import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.steps.HomePageSteps;
import com.kameleoon.selenium.tests.steps.globalHeaderSteps.MainPersoMenuSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;
import com.kameleoon.selenium.tests.steps.personalizationSteps.MyPersonalizationsSteps;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.junit.*;

import static com.kameleoon.selenium.tests.helpers.Login.login;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by roman on 14.03.16.
 */
public class CreateNewPersTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }


    @Test
    public void testCreateNewPersonalization(){
        login();
        new HomePageSteps().clickCreatePersonalizations();
        new UpdatePersonalizationSteps().createNewPers("test-CreateNewPersTest", "test-CreateNewPersTest");
        MyPersonalizationsSteps myPersonalizationsSteps = new MainPersoMenuSteps().clickMyPersoMenu();
        assertThat(myPersonalizationsSteps.getTileHeaderText(), is("TEST-CREATENEWPERSTEST"));
    }

    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
