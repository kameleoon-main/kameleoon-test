package com.kameleoon.selenium.tests.pages.widgetsPage.countdownBanner;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 21.03.16.
 *
 */
public class AboveCountdownBannerPage extends BasePage {
    public AboveCountdownBannerPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getBackToWidgetsButton().isDisplayed();
    }

    @FindBy(xpath = ".//*[contains(@class,'add_timer-above')]//*[@class='back_position']")
    private WebElement backToWidgetsButton;

    @FindBy(xpath = ".//*[contains(@class,'save_widget')]")
    private WebElement saveWidgetButton;

    //-- Alignment --//
    @FindBy(xpath = ".//*[contains(@class,'style_checkbox')]/*[contains(@value,'checkbox1')]/following::*[1]")
    private WebElement horizontalAlignmentCheckbox;

    @FindBy(xpath = ".//*[contains(@class,'style_checkbox')]/*[contains(@value,'checkbox1')]")
    private WebElement horizontalAlignmentChecked;

    @FindBy(xpath = ".//*[contains(@class,'style_checkbox')]/*[contains(@value,'checkbox2')]/following::*[1]")
    private WebElement verticalAlignmentCheckbox;

    @FindBy(xpath = ".//*[contains(@class,'style_checkbox')]/*[contains(@value,'checkbox2')]")
    private WebElement verticalAlignmentChecked;
    //---------------//


    public WebElement getBackToWidgetsButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",backToWidgetsButton);
        return backToWidgetsButton;
    }

    public WebElement getSaveWidgetButton() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",saveWidgetButton);
        return saveWidgetButton;
    }

    public WebElement getHorizontalAlignmentCheckbox() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",horizontalAlignmentCheckbox);
        return horizontalAlignmentCheckbox;
    }

    public WebElement getVerticalAlignmentCheckbox() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",verticalAlignmentCheckbox);
        return verticalAlignmentCheckbox;
    }

    public WebElement getHorizontalAlignmentChecked() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",horizontalAlignmentChecked);
        return horizontalAlignmentChecked;
    }

    public WebElement getVerticalAlignmentChecked() {
        ((JavascriptExecutor) WebDriverFactory.getDriver())
                .executeScript("arguments[0].scrollIntoView();",verticalAlignmentChecked);
        return verticalAlignmentChecked;
    }
}
