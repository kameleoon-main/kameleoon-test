package com.kameleoon.selenium.tests.pages;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rrog on 16.01.2016.
 *
 */
public class LoginPage extends BasePage {
    public LoginPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return signinButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='kamInputLogin']")
    private WebElement loginInputText;

    @FindBy(xpath = ".//*[@id='kamInputPassword']")
    private WebElement passwordInputText;

    @FindBy(xpath = ".//*[@id='kamBlocSignin']")
    private WebElement signinButton;

    @FindBy(xpath = ".//*[@class='InvalidFormMessage']")
    private WebElement invalidMessageText;

    @FindBy(xpath = ".//*[@class='ErrorMessage']")
    private WebElement errorMessageText;


    public WebElement getLoginInputText() {
        return loginInputText;
    }

    public WebElement getPasswordInputText(){
        return passwordInputText;
    }

    public WebElement getSigninButton(){
        return signinButton;
    }

    public WebElement getInvalidMessageText(){
        return invalidMessageText;
    }

    public WebElement getErrorMessageText() { return errorMessageText; }

}
