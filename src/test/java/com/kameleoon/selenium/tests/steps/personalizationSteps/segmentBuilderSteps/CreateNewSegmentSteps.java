package com.kameleoon.selenium.tests.steps.personalizationSteps.segmentBuilderSteps;

import com.kameleoon.selenium.tests.pages.personalizationPage.segmentBuilderPage.CreateNewSegmentPage;
import com.kameleoon.selenium.tests.steps.personalizationSteps.UpdatePersonalizationSteps;
import com.kameleoon.selenium.tests.utils.TimeUtils;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 18.03.16.
 *
 */
public class CreateNewSegmentSteps {
    private CreateNewSegmentPage createNewSegmentPage;
    public CreateNewSegmentSteps() {
        createNewSegmentPage = new CreateNewSegmentPage();
    }


    // TODO: Вместо этого метода написать в getNestableItem() поиск предка nameElement, а затем проверять открыт ли он
    private void openAccordion(String nameItem){
        List<WebElement> accordionList = createNewSegmentPage.getAccordionList();
        WebElement nameElement = null;
        switch (nameItem) {
            case "Targeted pages":
                nameElement = accordionList.get(0);
                break;
            case "Acquisition":
                nameElement = accordionList.get(1);
                break;
            case "Visitor characteristics":
                nameElement = accordionList.get(2);
                break;
            case "Visiting behavior":
                nameElement = accordionList.get(3);
                break;
            case "External environment":
                nameElement = accordionList.get(4);
                break;
            case "Custom data":
                nameElement = accordionList.get(5);
                break;
            case "Technical":
                nameElement = accordionList.get(6);
                break;
        }

        if (nameElement != null) {
            String attribute = nameElement.getAttribute("className");
            if (!attribute.contains("uk-active"))
                nameElement.click();
        }
        else throw new NotFoundException();
    }

    private WebElement getNestableItem(String nameItem){
        List<WebElement> nestableList = createNewSegmentPage.getNestableList();
        WebElement nameElement = null;
        for (WebElement element : nestableList) {
            if(element.getAttribute("data-type").contains(nameItem)) {
                nameElement = element;
            }
        }
        return nameElement;
    }

    public <PageObjectClass> PageObjectClass dragNestableItemToSegmentsArea(Class<PageObjectClass> pageObjectClass){
        openAccordion("Targeted pages");
        WebElement draggable = getNestableItem("PAGE_URL");
        WebElement target = createNewSegmentPage.getSegmentsArea();
        new Actions(WebDriverFactory.getDriver()).dragAndDrop(draggable, target).perform();
        TimeUtils.waitForSeconds(1);
        try {
            return page(pageObjectClass.getConstructor().newInstance());
        }
        catch (Exception e) {
            throw new RuntimeException("Failed to create new instance of " + pageObjectClass, e);
        }
    }

    public void inputNameAndDescriptionText(String name, String description){
        createNewSegmentPage.getSegmentNameInputText().sendKeys(name);
        createNewSegmentPage.getSegmentDescriptionInputText().sendKeys(description);
    }

    public UpdatePersonalizationSteps clickSave(){
        createNewSegmentPage.getValidateButton().click();
        TimeUtils.waitForSeconds(1);
        return page(UpdatePersonalizationSteps.class);
    }
}
