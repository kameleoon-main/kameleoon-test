package com.kameleoon.selenium.tests.steps.abTestSteps;

import com.kameleoon.selenium.tests.pages.abTestPage.AbtestMenuPage;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.AllVariationsSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.LaunchedTestsSteps;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 14.03.16.
 *
 */
public class AbtestMenuSteps {

    private AbtestMenuPage abtestMenuPage;
    public AbtestMenuSteps() {
        abtestMenuPage = new AbtestMenuPage();
    }

    public TestsInConfigurationSteps goToTestsInConfiguration(){
        abtestMenuPage.getAbtestMenuList().get(0).click();
        return page(TestsInConfigurationSteps.class);
    }

    public LaunchedTestsSteps goToLaunchedTests(){
        abtestMenuPage.getAbtestMenuList().get(1).click();
        return page(LaunchedTestsSteps.class);
    }

    public AllVariationsSteps goToAllVariations(){
        abtestMenuPage.getAbtestMenuList().get(2).click();
        return page(AllVariationsSteps.class);
    }
}
