package com.kameleoon.selenium.tests.helpers;

import com.kameleoon.selenium.tests.configuration.properties.Properties;
import com.kameleoon.selenium.tests.steps.LoginPageSteps;
import com.kameleoon.selenium.tests.steps.SignupPageSteps;
import com.kameleoon.selenium.tests.utils.DBConnectionManager;
import com.kameleoon.selenium.tests.webtestsbase.BasePage;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.open;

/**
 * Created by roman on 09.03.16.
 *
 */
public class Login {
    private static DBConnectionManager db = new DBConnectionManager();

    public static void login(){
        String email = selectEmailInDatabase();
        if (email!= null && !email.isEmpty()) {
            LoginPageSteps loginPageSteps = open(Properties.getUrl(), LoginPageSteps.class);
            loginPageSteps.loginAs(email, "Qwerty11");
            updateScriptInDatabase(email);
            updatePersonalizationInDatabase(email);
            updatePremiumSetupInDatabase(email);
            selectCodeInDatabase(email);
            BasePage.getDriver().navigate().refresh();
        }
        else {
            newLogin();
        }
    }

    public static void newLogin(){
        SignupPageSteps signupPageSteps = open(Properties.getUrl() + "/access/signup", SignupPageSteps.class);
        signupPageSteps.createNewAccount();
        updateScriptInDatabase(SignupPageSteps.email);
        updatePersonalizationInDatabase(SignupPageSteps.email);
        updatePremiumSetupInDatabase(SignupPageSteps.email);
        selectCodeInDatabase(SignupPageSteps.email);
        BasePage.getDriver().navigate().refresh();
    }




    public static void updateScriptInDatabase(String email){
        db.executeUpdate("UPDATE site SET active_script=TRUE WHERE name='" + email + "'");
    }

    public static void updatePersonalizationInDatabase(String email){
        db.executeUpdate("UPDATE customer SET personalization_setup=TRUE WHERE name='" + email + "'");
    }

    public static void updatePremiumSetupInDatabase(String email){
        db.executeUpdate("UPDATE customer SET abtesting_premium_setup=TRUE WHERE name='" + email + "'");
    }

    public static String selectEmailInDatabase(){
        return db.execute("SELECT email FROM person WHERE email LIKE '%@test.com' ORDER BY id DESC LIMIT 1");
    }

    public static String selectCodeInDatabase(String email){
        String code = db.execute("SELECT code FROM site WHERE name='"+ email +"'");
        JavaScript.setCode(code);
        return code;
    }

}