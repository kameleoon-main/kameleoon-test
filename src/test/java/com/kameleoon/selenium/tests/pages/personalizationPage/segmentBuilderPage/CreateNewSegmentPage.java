package com.kameleoon.selenium.tests.pages.personalizationPage.segmentBuilderPage;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

/**
 * Created by roman on 18.03.16.
 *
 */
public class CreateNewSegmentPage extends BasePage {
    public CreateNewSegmentPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getSegmentNameInputText().isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='segment-editor']//input[contains(@class,'segment-name')]")
    private WebElement segmentNameInputText;

    @FindBy(xpath = ".//*[@id='segment-editor']//input[contains(@class,'segment-description')]")
    private WebElement segmentDescriptionInputText;

    //-- Conditions --//
    @FindBys(@FindBy(xpath = ".//*[contains(@class,'targeting-group')]"))
    private List<WebElement> accordionList;
    //---------------//

    //-- Nestable List --//
    @FindBys(@FindBy(xpath = ".//*[@data-type]"))
    private List<WebElement> nestableList;
    //------------------//

    @FindBy(xpath = ".//*[@id='segment-editor']//*[@class='segments__area']")
    private WebElement segmentsArea;

    @FindBy(xpath = ".//*[@id='segment-editor']//*[contains(@class, 'validate-button')]")
    private WebElement validateButton;



    public WebElement getSegmentNameInputText() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,segmentNameInputText);
        return segmentNameInputText;
    }

    public WebElement getSegmentDescriptionInputText() {
        return segmentDescriptionInputText;
    }


    public List<WebElement> getAccordionList() {
        return accordionList;
    }

    public List<WebElement> getNestableList() {
        return nestableList;
    }

    public WebElement getSegmentsArea() {
        return segmentsArea;
    }

    public WebElement getValidateButton() {
        return validateButton;
    }
}
