package com.kameleoon.selenium.tests.pages;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 03.03.16.
 *
 */
public class SignupPage extends BasePage {
    public SignupPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return signupButton.isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='creerMonCompte']")
    private WebElement signupButton;

    @FindBy(xpath = ".//*[@id='company']")
    private WebElement companyInputText;

    @FindBy(xpath = ".//*[@id='firstname']")
    private WebElement firstnameInputText;

    @FindBy(xpath = ".//*[@id='email']")
    private WebElement emailInputText;

    @FindBy(xpath = ".//*[@id='telephone']")
    private WebElement telephoneInputText;

    @FindBy(xpath = ".//*[@id='lastname']")
    private WebElement lastnameInputText;

    @FindBy(xpath = ".//*[@id='password']")
    private WebElement passwordInputText;

    @FindBy(xpath = ".//*[@id='url']")
    private WebElement urlInputText;


    public WebElement getSignupButton () {
        return signupButton;
    }

    public WebElement getCompanyInputText () {
        return companyInputText;
    }

    public WebElement getFirstnameInputText () {
        return firstnameInputText;
    }

    public WebElement getEmailInputText () {
        return emailInputText;
    }

    public WebElement getTelephoneInputText () {
        return telephoneInputText;
    }

    public WebElement getLastnameInputText () {
        return lastnameInputText;
    }

    public WebElement getPasswordInputText () {
        return passwordInputText;
    }

    public WebElement getUrlInputText () {
        return urlInputText;
    }
}
