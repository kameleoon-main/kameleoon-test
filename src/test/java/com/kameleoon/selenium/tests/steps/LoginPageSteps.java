package com.kameleoon.selenium.tests.steps;

import com.kameleoon.selenium.tests.pages.LoginPage;
import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 01.03.16.
 *
 */
public class LoginPageSteps {

    private LoginPage loginPage;

    public LoginPageSteps(){
        loginPage = new LoginPage();
    }

    public <PageObjectClass> PageObjectClass loginAs(String login, String password,
                                                     Class<PageObjectClass> pageObjectClass){
        loginPage.getLoginInputText().sendKeys(login);
        loginPage.getPasswordInputText().sendKeys(password);
        loginPage.getSigninButton().click();
        return page(pageObjectClass);
    }

    public void loginAs(String login, String password){
        loginPage.getLoginInputText().sendKeys(login);
        loginPage.getPasswordInputText().sendKeys(password);
        loginPage.getSigninButton().click();
    }


    public String getInvalidMessageText(){
        return loginPage.getInvalidMessageText().getText();
    }

    public String getErrorMessageText() { return loginPage.getErrorMessageText().getText(); }

}
