package com.kameleoon.selenium.tests.steps.abTestSteps.popups;

import com.kameleoon.selenium.tests.pages.abTestPage.popups.PopupUpdateUrlPage;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import org.openqa.selenium.Keys;

import static com.kameleoon.selenium.tests.webtestsbase.BasePage.page;

/**
 * Created by roman on 14.03.16.
 */
public class PopupUpdateUrlSteps {

    private PopupUpdateUrlPage popupUpdateUrlPage;

    public PopupUpdateUrlSteps() {
        popupUpdateUrlPage = new PopupUpdateUrlPage();
    }

    public TestsInConfigurationSteps editUrl(String url) {
        popupUpdateUrlPage.getUrlInputText().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        popupUpdateUrlPage.getUrlInputText().sendKeys(Keys.BACK_SPACE);
        popupUpdateUrlPage.getUrlInputText().sendKeys(url);
        popupUpdateUrlPage.getOkButton().click();
        return page(TestsInConfigurationSteps.class);
    }
}
