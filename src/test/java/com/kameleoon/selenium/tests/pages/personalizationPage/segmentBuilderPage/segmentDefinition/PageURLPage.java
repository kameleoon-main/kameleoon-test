package com.kameleoon.selenium.tests.pages.personalizationPage.segmentBuilderPage.segmentDefinition;

import com.kameleoon.selenium.tests.webtestsbase.BasePage;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by roman on 18.03.16.
 *
 */
public class PageURLPage extends BasePage {
    public PageURLPage() {
        super();
    }

    @Override
    public boolean isPageOpened() {
        return getFirstValueInputText().isDisplayed();
    }

    @FindBy(xpath = ".//*[@id='segment-editor']//*[contains(@class,'first-value')]")
    private WebElement firstValueInputText;

    public WebElement getFirstValueInputText() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript("arguments[0].scrollIntoView();"
                ,firstValueInputText);
        return firstValueInputText;
    }
}
