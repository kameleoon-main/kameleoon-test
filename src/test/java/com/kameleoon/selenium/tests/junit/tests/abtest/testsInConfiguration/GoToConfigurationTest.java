package com.kameleoon.selenium.tests.junit.tests.abtest.testsInConfiguration;

import com.kameleoon.selenium.tests.configuration.properties.Properties;
import com.kameleoon.selenium.tests.steps.*;
import com.kameleoon.selenium.tests.steps.abTestSteps.configurationSteps.TestsInConfigurationSteps;
import com.kameleoon.selenium.tests.webtestsbase.BasePage;

import static com.kameleoon.selenium.tests.helpers.Login.updatePersonalizationInDatabase;
import static com.kameleoon.selenium.tests.helpers.Login.updateScriptInDatabase;
import static com.kameleoon.selenium.tests.webtestsbase.BasePage.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.*;
import com.kameleoon.selenium.tests.junit.rules.DBConnectionRule;
import com.kameleoon.selenium.tests.junit.rules.ScreenShotOnFailRule;
import com.kameleoon.selenium.tests.webtestsbase.WebDriverFactory;

/**
 * Created by roman on 10.03.16.
 */
public class GoToConfigurationTest {
    @ClassRule
    public static com.kameleoon.selenium.tests.junit.rules.DBConnectionRule DBConnectionRule = new DBConnectionRule();

    @Rule
    public ScreenShotOnFailRule screenShotOnFailRule = new ScreenShotOnFailRule();

    @BeforeClass
    public static void beforeTest() {
        WebDriverFactory.startBrowser(true);
    }

    @Test
    public void testGoToConfigurationTests(){
        //Create new account
        SignupPageSteps signupPageSteps = open(Properties.getUrl() + "/access/signup", SignupPageSteps.class);
        signupPageSteps.createNewAccount();

        //Enable ActiveScript and Personalization
        updateScriptInDatabase(SignupPageSteps.email);
        updatePersonalizationInDatabase(SignupPageSteps.email);

        //Refresh page
        BasePage.getDriver().navigate().refresh();

        TestsInConfigurationSteps testsInConfigurationSteps = new HomePageSteps().clickCreateTest();
        assertThat(testsInConfigurationSteps.getTitleActionText(), equalTo("Tests en configuration"));
    }



    @AfterClass
    public static void afterTest() {
        WebDriverFactory.finishBrowser();
    }
}
